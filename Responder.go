package httpx

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

// A Responder is used to form and send responses to HTTP requests.
type Responder struct {
	cfg *Config
	w   http.ResponseWriter
}

// NewResponder creates a new responder from the provided configuration and
// response writer.
func NewResponder(cfg *Config, w http.ResponseWriter) *Responder {
	return &Responder{
		cfg: cfg,
		w:   w,
	}
}

// Respond simply responds to the request with the provided statusCode and
// nothing else.
func (res *Responder) Respond(statusCode int) {
	res.w.WriteHeader(statusCode)
}

// RespondWithBytes responds to the request with the provided byte slice and
// content type.
func (res *Responder) RespondWithBytes(statusCode int, data []byte, contentType string) {
	res.w.Header().Set("Content-Type", contentType)
	res.w.Header().Set("Content-Length", fmt.Sprintf("%v", len(data)))
	res.Respond(statusCode)
	res.w.Write(data)
}

// RespondWithJSON responds to the request with a JSON model and the provided
// status code.
func (res *Responder) RespondWithJSON(statusCode int, model interface{}) {
	if model == nil {
		res.w.WriteHeader(statusCode)
		return
	}

	raw, err := json.Marshal(model)
	if err != nil {
		errText := ""
		if res.cfg.DebuggingEnabled {
			errText = fmt.Sprintf(`,"error":%v`, strconv.Quote(err.Error()))
		}

		statusCode = http.StatusInternalServerError
		raw = []byte(fmt.Sprintf(`{"type":"%v","description":"%v"%v}`, ProblemResponseModelSerialization, "The response to the operation could not be serialized.", errText))
	}

	res.RespondWithBytes(statusCode, raw, "application/json")
}
