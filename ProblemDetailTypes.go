package httpx

// The set of possible ProblemDetailTypes.
const (
	ProblemResponseModelSerialization = "RESPONSE_MODEL_SERIALIZATION"
	ProblemUnsupportedMediaType       = "UNSUPPORTED_MEDIA_TYPE"
	ProblemLengthRequired             = "LENGTH_REQUIRED"
	ProblemRequestEntityTooLarge      = "REQUEST_ENTITY_TOO_LARGE"
	ProblemInvalidJSON                = "INVALID_JSON"
	ProblemUnprocessableEntity        = "UNPROCESSABLE_ENTITY"
	ProblemInternalServerError        = "INTERNAL_SERVER_ERROR"
)
