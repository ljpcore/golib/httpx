package httpx

import (
	"encoding/json"
	"net/http"
	"strings"
)

// A Receiver is used to read and inspect incoming requests.  It uses a
// responder to automatically respond to requests in some cases.
type Receiver struct {
	cfg       *Config
	r         *http.Request
	responder *Responder
}

// NewReceiver creates a new Receiver from the provided configuration, request
// and responder.
func NewReceiver(cfg *Config, r *http.Request, responder *Responder) *Receiver {
	return &Receiver{
		cfg:       cfg,
		r:         r,
		responder: responder,
	}
}

// FromJSON reads the JSON body of the request into the provided model.  It
// requires that the request:
//
// - Uses Content-Type: application/json
// - Specifies a Content-Length within the bounds of the value specified in
//   config.
// - Provides a valid JSON body that passes sanitization.
//
// If any of the above fail, FromJSON will handle the response and return false.
// If the model is read successfully, FromJSON will return true.
func (recv *Receiver) FromJSON(model Sanitizable) bool {
	if !recv.AssertContentType("application/json") {
		return false
	}

	if !recv.AssertContentLength(recv.cfg.MaximumJSONRequestBodyLength) {
		return false
	}

	decoder := json.NewDecoder(recv.r.Body)
	if err := decoder.Decode(model); err != nil {
		problem := getProblemDetailsForInvalidJSON()
		recv.responder.RespondWithJSON(http.StatusBadRequest, problem)
		return false
	}

	if field, err := model.Sanitize(); err != nil {
		problem := getProblemDetailsForUnprocessableEntity(field, err)
		recv.responder.RespondWithJSON(http.StatusUnprocessableEntity, problem)
		return false
	}

	return true
}

// AssertContentType asserts that the associated request uses one of the
// provided content types.  If it does not, AssertContentType responds with a
// 415 Unsupported Media Type detailing the problem and returns false.
func (recv *Receiver) AssertContentType(contentTypes ...string) bool {
	providedContentType := strings.ToLower(recv.r.Header.Get("Content-Type"))

	for _, contentType := range contentTypes {
		allowedContentType := strings.ToLower(contentType)
		if providedContentType == allowedContentType {
			return true
		}
	}

	problem := getProblemDetailsForUnsupportedMediaType(providedContentType, contentTypes)
	recv.responder.RespondWithJSON(http.StatusUnsupportedMediaType, problem)

	return false
}

// AssertContentLength asserts that the associated request provides a content
// length and that it is no more than the provided maximum number of bytes.  If
// the assertion fails, AssertContentLength responds with either a 411 Length
// Required or a 413 Request Entity Too Large and returns false.
func (recv *Receiver) AssertContentLength(maxLength int64) bool {
	if recv.r.ContentLength < 1 {
		problem := getProblemDetailsForLengthRequired()
		recv.responder.RespondWithJSON(http.StatusLengthRequired, problem)
		return false
	}

	if recv.r.ContentLength > maxLength {
		problem := getProblemDetailsForRequestEntityTooLarge()
		recv.responder.RespondWithJSON(http.StatusRequestEntityTooLarge, problem)
		return false
	}

	return true
}

// AssertNilError asserts that the provided error is nil.  If it is not,
// AssertNilError will return a 500 Internal Server Error and, if debugging is
// enabled, return the error in the response.  The error is not returned when
// debugging is disabled.
func (recv *Receiver) AssertNilError(err error) bool {
	if err == nil {
		return true
	}

	problem := getProblemDetailsForInternalServerError()
	if recv.cfg.DebuggingEnabled {
		problem.Error = err.Error()
	}

	recv.responder.RespondWithJSON(http.StatusInternalServerError, problem)
	return false
}

func getProblemDetailsForUnsupportedMediaType(providedContentType string, allowedContentTypes []string) *ProblemDetails {
	return &ProblemDetails{
		Type:        ProblemUnsupportedMediaType,
		Description: `Expected the Content-Type header to be "application/json".`,
		Specifics: map[string]interface{}{
			"allowedContentTypes": allowedContentTypes,
			"providedContentType": providedContentType,
		},
	}
}

func getProblemDetailsForLengthRequired() *ProblemDetails {
	return &ProblemDetails{
		Type:        ProblemLengthRequired,
		Description: "Expected the Content-Length header to be a positive integer.",
	}
}

func getProblemDetailsForRequestEntityTooLarge() *ProblemDetails {
	return &ProblemDetails{
		Type:        ProblemRequestEntityTooLarge,
		Description: "The provided request body is too large.",
	}
}

func getProblemDetailsForInvalidJSON() *ProblemDetails {
	return &ProblemDetails{
		Type:        ProblemInvalidJSON,
		Description: "The provided JSON body is invalid or otherwise unable to be deserialized.",
	}
}

func getProblemDetailsForUnprocessableEntity(field string, err error) *ProblemDetails {
	return &ProblemDetails{
		Type:        ProblemUnprocessableEntity,
		Description: "One or more fields of the request model could not be processed.",
		Specifics: map[string]string{
			"field":  field,
			"reason": err.Error(),
		},
	}
}

func getProblemDetailsForInternalServerError() *ProblemDetails {
	return &ProblemDetails{
		Type:        ProblemInternalServerError,
		Description: "An internal server error prevented the operation from completing.",
	}
}
