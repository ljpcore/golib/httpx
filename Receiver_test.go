package httpx

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
)

func SetupReceiverTest() (*Receiver, *httptest.ResponseRecorder) {
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, "/", bytes.NewBufferString(`{"firstName":" john "}`))
	r.Header.Set("Content-Type", "application/json")

	cfg := &Config{
		DebuggingEnabled:             true,
		MaximumJSONRequestBodyLength: 22,
	}

	return NewReceiver(cfg, r, NewResponder(cfg, w)), w
}

func TestReceiverFromJSONSuccess(t *testing.T) {
	// Arrange.
	receiver, w := SetupReceiverTest()

	// Act.
	model := &FirstNameModel{}
	success := receiver.FromJSON(model)

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusOK))
	test.That(t, success, is.True)
	test.That(t, model.FirstName, is.EqualTo("John"))
}

func TestReceiverFromJSONFailBadContentType(t *testing.T) {
	// Arrange.
	receiver, w := SetupReceiverTest()
	receiver.r.Header.Del("Content-Type")

	// Act.
	model := &FirstNameModel{}
	success := receiver.FromJSON(model)

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusUnsupportedMediaType))
	test.That(t, success, is.False)
	test.That(t, model.FirstName, is.EqualTo(""))
}

func TestReceiverFromJSONMissingContentLength(t *testing.T) {
	// Arrange.
	receiver, w := SetupReceiverTest()
	receiver.r = httptest.NewRequest(http.MethodPost, "/", nil)
	receiver.r.Header.Set("Content-Type", "application/json")

	// Act.
	model := &FirstNameModel{}
	success := receiver.FromJSON(model)

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusLengthRequired))
	test.That(t, success, is.False)
	test.That(t, model.FirstName, is.EqualTo(""))
}

func TestReceiverFromJSONTooLarge(t *testing.T) {
	// Arrange.
	receiver, w := SetupReceiverTest()
	receiver.r = httptest.NewRequest(http.MethodPost, "/", bytes.NewBufferString(`{"firstName":" john  "}`))
	receiver.r.Header.Set("Content-Type", "application/json")

	// Act.
	model := &FirstNameModel{}
	success := receiver.FromJSON(model)

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusRequestEntityTooLarge))
	test.That(t, success, is.False)
	test.That(t, model.FirstName, is.EqualTo(""))
}

func TestReceiverFromJSONInvalid(t *testing.T) {
	// Arrange.
	receiver, w := SetupReceiverTest()
	receiver.r = httptest.NewRequest(http.MethodPost, "/", bytes.NewBufferString(`{"firstName"" john "}`))
	receiver.r.Header.Set("Content-Type", "application/json")

	// Act.
	model := &FirstNameModel{}
	success := receiver.FromJSON(model)

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusBadRequest))
	test.That(t, success, is.False)
	test.That(t, model.FirstName, is.EqualTo(""))
}

func TestReceiverFromJSONFirstNameTooLong(t *testing.T) {
	// Arrange.
	receiver, w := SetupReceiverTest()
	receiver.r = httptest.NewRequest(http.MethodPost, "/", bytes.NewBufferString(`{"firstName":" johns"}`))
	receiver.r.Header.Set("Content-Type", "application/json")

	// Act.
	model := &FirstNameModel{}
	success := receiver.FromJSON(model)

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusUnprocessableEntity))
	test.That(t, success, is.False)
	test.That(t, model.FirstName, is.EqualTo("Johns"))
}

func TestReceiverAssertNilErrorWhenNotNil(t *testing.T) {
	// Arrange.
	receiver, w := SetupReceiverTest()

	// Act.
	success := receiver.AssertNilError(errors.New("something failed"))

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusInternalServerError))
	test.That(t, success, is.False)

	rawBody, err := io.ReadAll(w.Result().Body)
	test.That(t, err, is.Nil)

	problemDetailsModel := &ProblemDetails{}
	err = json.Unmarshal(rawBody, problemDetailsModel)
	test.That(t, err, is.Nil)
	test.That(t, problemDetailsModel.Error, is.EqualTo("something failed"))
}

func TestReceiverAssertNilErrorWhenNil(t *testing.T) {
	// Arrange.
	receiver, w := SetupReceiverTest()

	// Act.
	success := receiver.AssertNilError(nil)

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusOK))
	test.That(t, success, is.True)
}

// --

type FirstNameModel struct {
	FirstName string `json:"firstName"`
}

var _ Sanitizable = &FirstNameModel{}

func (m *FirstNameModel) Sanitize() (string, error) {
	m.FirstName = strings.Title(strings.TrimSpace(m.FirstName))

	if len(m.FirstName) > 4 {
		return "firstName", errors.New("firstName limit of 4 characters")
	}

	return "", nil
}
