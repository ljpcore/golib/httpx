package httpx

// Config defines a small set of configuration options that define how certain
// types in this package behave.
type Config struct {
	// DebuggingEnabled causes information that would normally not be exposed in
	// production to be sent in responses.  Useful for test environments.
	DebuggingEnabled bool

	// MaximumJSONRequestBodyLength defines the maximum size allowed for a
	// request JSON body when using FromJSON.
	MaximumJSONRequestBodyLength int64
}
