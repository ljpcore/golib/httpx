package httpx

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/has"
	"gitlab.com/ljpcore/golib/test/is"
)

func SetupResponderTest(debuggingEnabled bool) (*Responder, *httptest.ResponseRecorder) {
	cfg := &Config{DebuggingEnabled: debuggingEnabled}

	w := httptest.NewRecorder()

	return NewResponder(cfg, w), w
}

func TestResponderRespondWithJSONMarshalFailNonDebugging(t *testing.T) {
	// Arrange.
	responder, w := SetupResponderTest(false)

	// Act.
	responder.RespondWithJSON(http.StatusCreated, &Nonmarshallable{})

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusInternalServerError))

	body, err := io.ReadAll(w.Body)
	test.That(t, err, is.Nil)
	test.That(t, string(body), is.EqualTo(`{"type":"RESPONSE_MODEL_SERIALIZATION","description":"The response to the operation could not be serialized."}`))
}

func TestResponderRespondWithJSONMarshalFailDebugging(t *testing.T) {
	// Arrange.
	responder, w := SetupResponderTest(true)

	// Act.
	responder.RespondWithJSON(http.StatusCreated, &Nonmarshallable{})

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusInternalServerError))

	body, err := io.ReadAll(w.Body)
	test.That(t, err, is.Nil)
	test.That(t, string(body), is.EqualTo(`{"type":"RESPONSE_MODEL_SERIALIZATION","description":"The response to the operation could not be serialized.","error":"json: error calling MarshalJSON for type *httpx.Nonmarshallable: cannot be marshalled"}`))
}

func TestResponderRespondWithJSONSuccess(t *testing.T) {
	// Arrange.
	responder, w := SetupResponderTest(true)

	// Act.
	responder.RespondWithJSON(http.StatusCreated, &struct{ FirstName string }{FirstName: "Luke"})

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusCreated))
	test.That(t, w.Header().Get("Content-Type"), is.EqualTo("application/json"))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo("20"))

	body, err := io.ReadAll(w.Body)
	test.That(t, err, is.Nil)
	test.That(t, string(body), is.EqualTo(`{"FirstName":"Luke"}`))
}

func TestResponderRespondWithJSONSuccessNilModel(t *testing.T) {
	// Arrange.
	responder, w := SetupResponderTest(true)

	// Act.
	responder.RespondWithJSON(http.StatusConflict, nil)

	// Assert.
	test.That(t, w.Code, is.EqualTo(http.StatusConflict))
	test.That(t, w.Header().Get("Content-Type"), is.EqualTo(""))
	test.That(t, w.Header().Get("Content-Length"), is.EqualTo(""))

	body, err := io.ReadAll(w.Body)
	test.That(t, err, is.Nil)
	test.That(t, body, has.Length(0))
}

// --

// Nonmarshallable cannot be marshalled.
type Nonmarshallable struct{}

var _ json.Marshaler = &Nonmarshallable{}

// MarshalJSON always returns an error.
func (n *Nonmarshallable) MarshalJSON() ([]byte, error) {
	return nil, errors.New("cannot be marshalled")
}
