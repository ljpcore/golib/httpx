package httpx

// Sanitizable is implemented by types that are capable of sanitizing and
// validating themselves.  Mainly used by incoming HTTP request models.
type Sanitizable interface {
	// Should tidy and validate fields e.g. trim spaces off strings, check
	// lengths.  If an error is returned, the model could not be sanitized and
	// the returned string is the name of the first offending field.
	Sanitize() (string, error)
}
