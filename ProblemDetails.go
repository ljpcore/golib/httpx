package httpx

// The ProblemDetails structure represents a standard error reporting type that
// should be returned from any failed operation.  Can be emitted both at the
// HTTP level and at the application level.
type ProblemDetails struct {
	// The type of the problem.  This is field can influence the behavior of
	// downstream consumers.  Should be succinct and not change over time e.g.
	// "INVALID_OPERATION" etc.
	Type string `json:"type"`

	// The human-readable full description of the problem.  Has the potential to
	// be displayed to end-users if the downstream consumer does not know how
	// to handle the specified Type. e.g. "The operation could not complete
	// successfully as the method is unsupported." etc.
	Description string `json:"description"`

	// Any specific pieces of information relevant to the error.  Can be used
	// to influence behavior of downstream consumers.
	Specifics interface{} `json:"specifics,omitempty"`

	// The original error, if any, that caused the ProblemDetails to be raised.
	// Will be exposed by the API iff debugging is enabled.
	Error string `json:"error,omitempty"`
}
