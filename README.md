![](icon.png)

# httpx

Useful helpers and shortcuts for reading and writing HTTP requests and
responses.

---

Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from
[www.flaticon.com](https://www.flaticon.com/).
